package com.gcu.Models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

/**
 * This model holds the login information for any login form along with providing data validation outlines
 * @author joshbeck
 *
 */
public class LoginModel {
	@NotNull(message="User name must not be empty")
	@Size(min=1, max=32, message="User name must be between 1 and 32 characters")
	private String username;
	
	@NotNull(message="Password must not be empty")
	@Size(min=1, max=32, message="Password must be between 1 and 32 characters")
	private String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
