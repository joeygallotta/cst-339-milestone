package com.gcu.Models;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

/**
 * This class is a model class that holds all information pertaining to a user.
 * In addition to providing data structure, it also provides an outline of the appropriate
 * data validation for the Spring framework to check
 * @author joshbeck
 *
 */
public class User {

	//Properties
	@NotNull
	private Long id;
	
	@NotNull(message="You must include a first name")
	@Length(min=1, max=20, message="The first name must be between 1 and 20 characters long")
	private String firstName;

	@NotNull(message="You must include a last name")
	@Length(min=1, max=20, message="The last name must be between 1 and 20 characters long")
	private String lastName;

	@NotNull(message="You must include an email")
	private String email;

	@NotNull(message="You must include a phone number")
	@Length(min=7, max=10, message="The phone number must be between 7-10 numbers long")
	private String phoneNum;

	@NotNull(message="You must include a username")
	@Length(min=10, max=50, message="The username must be between 10 and 50 characters long")
	private String username;
	
	@NotNull(message="You must include a password")
	@Length(min=10, max=50, message="The password must be between 10 and 50 characters long")
	private String password;
	
	//Getter/setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	//Constructors
	public User(Long id, String firstName, String lastName, String email, String phoneNum, String username,
			String password) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNum = phoneNum;
		this.username = username;
		this.password = password;
	}
	public User() {
		super();
	}
	
	
}
