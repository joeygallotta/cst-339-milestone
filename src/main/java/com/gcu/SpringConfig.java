package com.gcu;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gcu.Models.LoginModel;
import com.gcu.Models.SongModel;
import com.gcu.Models.User;
import com.gcu.Services.SongService;
import com.gcu.Services.UserService;

@Configuration
public class SpringConfig {
	@Bean(name="getUserService")
	public UserService getService() {
		return new UserService();
	}
	
	@Bean(name="getUserModel")
	@RequestScope
	public User getModel() {
		return new User();
	}
	
	@Bean(name="getLoginModel")
	@RequestScope
	public LoginModel getLoginModel() {
		return new LoginModel();
	}
	
	@Bean(name="getSongModel")
	@RequestScope
	public SongModel getSongModel() {
		return new SongModel();
	}
	
	@Bean(name="getSongService")
	public SongService getSongService() {
		return new SongService();
	}
}
