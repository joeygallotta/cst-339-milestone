package com.gcu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
public class Milestone2Application {
	@GetMapping("/")
	public String viewHomePage() {
		return "index";
	}
	public static void main(String[] args) {
		System.out.println("Wiw");
		SpringApplication.run(Milestone2Application.class, args);
	}

}
