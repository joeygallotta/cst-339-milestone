package com.gcu.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gcu.Models.SongModel;
import com.gcu.Models.User;
import com.gcu.Services.SongService;
import com.gcu.Services.UserService;

/**
 * This class is in charge of handling iterations with the product
 * @author joshbeck
 *
 */
@Controller
@RequestMapping("/songs")
public class ProductController {
	
	//Inject a singleton instance of the song service
	@Autowired
	SongService songService;
		
	//Inject a request scope instance of an empty song model
	@Autowired
	SongModel emptySong;
		
	/**
	 * Handle displaying the products
	 * @return the name of the product display page
	 */
	@GetMapping("")
	public String displaySongs() {
		return "product_page";
	}
	
	@GetMapping("/add")
	public String displayNewSong(Model model) {
		model.addAttribute("songModel", emptySong);
		return "add_new_song";
	}
	
	@PostMapping("/add/handler")
	public String handleRegistration(SongModel songModel, BindingResult bindingResult, Model model) {
		//@TODO: Registration handler simulation
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("loginMsg", "A few errors with your entry");
			model.addAttribute("songModel", songModel);
			return "add_new_song";
		}
		
		
		if (songService.add(songModel)) {
			return "song_add_success";
		} else {
			return "song_add_failure";
		}
	}
}
