package com.gcu.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gcu.Models.LoginModel;
import com.gcu.Models.User;
import com.gcu.Services.UserService;

/**
 * This class is the controller class for all logging in/logging out activities
 * It's main job is to route the appropriate URLs to and from the correct Thymeleaf pages
 * as well as appropriately communicate with the Service and Model classes to retrieve and store data
 * @author joshbeck
 *
 */
@Controller
@RequestMapping("/login")
public class LoginController {
	
	//Inject a singleton instance of the user service
	@Autowired
	UserService userService;
	
	//Inject a request scope instance of an empty user login model
	@Autowired
	LoginModel emptyUser;
	
	//The default URL under the /login sub URL
	@GetMapping("")
	//Handle presenting the login form by populating it with an empty user object
	public String login(Model model) {
		model.addAttribute("user", emptyUser);
		return "login";
	}
	
	//Handle the login form submission.  Be sure to encrypt the password with a hash to compare to the hashed passwords in the database
	@PostMapping("/handler")
	public String handleLogin(@Valid LoginModel loginModel, BindingResult bindingResult, HttpServletResponse httpServlet, Model model) throws IOException {
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("loginMsg", "A few errors with your entry");
			model.addAttribute("user", loginModel);
			return "login";
		}
		
		//Access the User Service class
		
		if (userService.login(loginModel)) {
			//If the login was successful, redirect to the product page
			 httpServlet.sendRedirect("/songs");
			 return null;
		} else {
			//The login was unsuccessful so must direct the webpage to the login failure page
			model.addAttribute("loginMsg", "Username or password did not work");
			model.addAttribute("user", loginModel);
			return "login";
		}
	}
	
	//Logout the user and redirect the user to the login page
	@GetMapping("/logout")
	public String logout() {
		//Access the User Service to logout the user
		userService.logout();
		//Redirect the user to the login page
		return "redirect:";
	}
}
