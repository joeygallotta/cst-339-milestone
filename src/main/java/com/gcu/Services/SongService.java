package com.gcu.Services;

import javax.naming.spi.ObjectFactory;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gcu.Models.LoginModel;
import com.gcu.Models.SongModel;
import com.gcu.Models.User;

/**
 * This service acts as a communicator between the data access classes and the controller classes.
 * Specifically, this service is focused on communication regarding the song fethcing/adding requests between the database and the ProductController.
 * As of Milestone 2, this service merely emulates database access. 
 * @author joshbeck
 *
 */
@Service
public class SongService {

	/**
	 * This method handles adding a new song to the database.  As of Milestone 2, it will return success every time as it is emulating the database
	 */
	public boolean add(SongModel song) {
		return true;
	}
	
}
