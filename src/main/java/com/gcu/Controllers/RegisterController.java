package com.gcu.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.gcu.Models.LoginModel;
import com.gcu.Models.User;
import com.gcu.Services.UserService;

/**
 * This class is in charge of handling all registration logic and routing
 * @author joshbeck
 *
 */
@Controller
@RequestMapping("/register")
public class RegisterController {
	
	//Inject a singleton instance of the user service
	@Autowired
	UserService userService;
	
	//Inject a request scope instance of an empty user model
	@Autowired
	User emptyUser;
	
	/**
	 * This method returns a link to the registration form while 
	 * inserting a model of an empty user object
	 * @param model - Provided by Spring Boot
	 * @return a link to the registration form
	 */
	@GetMapping("")
	public String showRegistrationForm(Model model) {
		model.addAttribute("user", emptyUser);
		return "registration_form";
	}
	
	/**
	 * This method handles the registration form's post.  It will handle any
	 * errors in data formatting and pass the form on to the appropriate HTML page
	 * based on the input from the service class
	 * @param user
	 * @return success or failure registration HTML page
	 */
	@PostMapping("/handler")
	public String handleRegistration(User user, BindingResult bindingResult, Model model) {
		//@TODO: Registration handler simulation
		
		if (bindingResult.hasErrors()) {
			model.addAttribute("loginMsg", "A few errors with your entry");
			model.addAttribute("user", user);
			return "registration_form";
		}
		
		
		if (userService.register(user)) {
			return "registration_success";
		} else {
			return "registration_failure";
		}
	}

}