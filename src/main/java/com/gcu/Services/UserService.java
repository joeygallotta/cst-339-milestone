package com.gcu.Services;

import javax.naming.spi.ObjectFactory;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.gcu.Models.LoginModel;
import com.gcu.Models.User;

/**
 * This service acts as a communicator between the data access classes and the controller classes.
 * Specifically, this service is focused on communication regarding the user profile between the database and the Login/RegisterControllers.
 * As of Milestone 2, this service merely emulates database access. 
 * @author joshbeck
 *
 */
@Service
public class UserService {

	/**
	 * This method handles the login logic and interacts with the database.  As of Milestone 2
	 * login logic is simulated by checking it the name is "joseph_smith".  If it is, continue
	 * with a successful login and if it is not, return a failure login.
	 * @param user - Passed from the Controller class
	 * @return - TRUE for successful login and FALSE for unsuccessful login
	 */
	public boolean login(@Valid LoginModel user) {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		
		if (user.getUsername().toLowerCase().equals("joseph_smith")) {
			session.setAttribute("loggedIn", true);
			return true;
		} else {
			System.out.println("GOT THIS FAR");
			session.setAttribute("loggedIn", false);
			return false;
		}
	}
	/**
	 * This method handles validating registration of a user and ensuring a complete DB
	 * save.  As of Milestone 2, it emulates interacting with a database by ensuring 
	 * the registration user name is NOT joseph_smith.  If it is, it will return a failure
	 * @param user
	 * @return FALSE if username is joseph_smith, TRUE if username is not joseph_smith
	 */
	public boolean register(User user) {
		if (user.getUsername() != "joseph_smith") {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This method handles interacting with the session to remove the "loggedIn" token
	 * connoting that a user has logged in within the session.
	 */
	public void logout() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		session.setAttribute("loggedIn", false);
	}
	
	/**
	 * This method returns whether the session variable indicates the user is logged in
	 * @return - Whether the user has logged in within the current session
	 */
	public boolean isUserLoggedIn() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();
		return session.getAttribute("loggedIn") == null ? false : (boolean)session.getAttribute("loggedIn");
	}
}
